// Copyright © 2019 Assured Information Security, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

class Menu {
    constructor(id, onLoad, onNavigate) {
        this.window = document.getElementById(id);

        let self = this;

        let getChildren = (node) => {
            node.childNodes.forEach((child) => {
                if (child.id != "") {
                    self[child.id] = child;
                }

                if(child.childNodes.length) {
                    getChildren(child);
                }
            });
        };

        getChildren(this.window);

        this.onLoad = onLoad;
        this.onNavigate = onNavigate;
    }

    load() {
        this.window.style.display = "flex";
        if (this.onLoad) {
            this.onLoad(this);
        }
    }

    hide() {
        this.window.style.display = "none";
    }

    navigate() {
        if (this.onNavigate) {
            return this.onNavigate(this);
        }

        return true;
    }
}

let installer = {
    menus: [
        new Menu(
            "welcomeMenu",
            (menu) => {
                menu.welcomeTitle.textContent =
                `Welcome to ${installer.state.productName}`;
            }
        ),
        new Menu(
            "credentialsMenu",
            undefined,
            (menu) => {
                let username = menu.usernameField.value;
                username = username ? username : "";

                let password = menu.passwordField.value;
                let confirmPassword = menu.confirmPasswordField.value;

                if (username.length < 33 && username.length > 3) {
                    if (checkPasswords(password, confirmPassword)) {
                        installer.state.username = username;
                        installer.state.password = password;

                        return true;
                    }
                } else {
                    installer.showError("Enter a username between 4-32 characters");
                }

                return false;
            }
        ),
        new Menu(
            "diskMenu",
            (menu) => {
                if (!menu.loaded) {
                    installer.state.devices.forEach((device) => {
                        let option = document.createElement("option");
                        option.textContent = device;
                        option.value = device;
                        menu.diskField.appendChild(option);
                    });

                    menu.loaded = true;
                }

                toggleEncryptionPassword();
            },
            (menu) => {
                if (menu.diskField.value != "Select a disk") {
                    installer.state.blockDevice = menu.diskField.value;

                    let password = menu.encryptPasswordField.value;
                    let confirmPassword = menu.encryptPasswordConfirm.value;

                    if (!menu.encryptField.checked) {
                        installer.state.diskCrypts = [];
                    } else {
                        let setAuthModeOptions = (pw) => ([
                            {
                                Key: "key",
                                Value: pw
                            },
                            {
                                Key: "key_type",
                                Value: "string"
                            }
                        ]);

                        let cryptIndex = installer.state.diskCrypts
                            .findIndex((crypt) => (crypt.TargetName === "storage"));

                        if (password != "") {
                            if (!checkPasswords(password, confirmPassword)) {
                                return false;
                            }

                            installer.state.diskCrypts[cryptIndex].AuthModeOptions = setAuthModeOptions(password);
                        } else {
                            installer.state.diskCrypts[cryptIndex].AuthModeOptions = setAuthModeOptions(installer.state.password);
                        }
                    }

                    return true;
                }

                installer.showError("Select a device");

                return false;
            }
        ),
        new Menu(
            "progressMenu",
            (menu) => {
                menu.progressTitle.textContent = `Installing ${installer.state.productName} ...`;
                toggleButton(installer.prevButton, true);
                toggleButton(installer.nextButton, true);
            },
            undefined
        )
    ],
    selectedMenu: 0,
    prevButton: document.getElementById("prevButton"),
    nextButton: document.getElementById("nextButton"),
    errorMessage: document.getElementById("error"),
    showError: function(message) {
        if (message) {
            this.errorMessage.textContent = message;
            this.errorMessage.style.display = "inline";
            return;
        }

        this.errorMessage.style.display = "none";
    },
    init: function() {
        let self = this;

        this.app.onMessage((message) => {
            info = JSON.parse(message);

            if (info.type && info.data) {
                    switch (info.type) {
                    case "state":
                        self.state = info.data;
                        self.selectedMenu = 0;
                        self.navigate();
                        break;
                    case "progress":
                        updateProgress(info.data.percentage, info.data.message);
                        break;
                    case "error":
                        installer.selectedMenu = installer.menus.length - 1;
                        installer.navigate();
                        installer.menus[installer.selectedMenu].progressTitle.textContent = "Something went wrong";
                        installer.showError(info.data);
                        finalize();
                }
            }
        });
    },
    navigate: function(direction) {
        let self = this;
        error = false;

        if (direction == "back" && this.selectedMenu != 0) {
            this.menus[this.selectedMenu].hide();
            this.selectedMenu = this.selectedMenu - 1;
        } else if (direction == "next" && this.selectedMenu != this.menus.length - 1) {
            error = !this.menus[this.selectedMenu].navigate()

            if (!error) {
                this.menus[this.selectedMenu].hide();
                this.selectedMenu = this.selectedMenu + 1;

                this.errorMessage.textContent = "";
            }
        }

        toggleButton(this.prevButton, (this.selectedMenu == 0));

        if (this.selectedMenu == this.menus.length - 2) {
            self.nextButton.textContent = "Submit";
            self.nextButton.onclick = function() { self.submit(); };
        } else if (this.selectedMenu == this.menus.length - 1) {
            self.prevButton.disabled = true;
            self.nextButton.textContent = "Finish";
            self.nextButton.onclick = function () { self.finish(); }
        } else {
            self.prevButton.onclick = function() { self.navigate("back"); }

            self.nextButton.textContent = "Next";
            self.nextButton.onclick = function() { self.navigate("next"); }
        }

        if (!error) {
            installer.showError();
            this.menus[this.selectedMenu].load();
        }

    },
    submit: function() {
        if (this.menus[this.selectedMenu].navigate()) {
            installer.app.sendMessage({ type: "submit", data: this.state });
            installer.navigate("next");
        }
    },
    finish: function() {
        installer.app.sendMessage({ type: "finish" });
    }
};

function init() {
    document.addEventListener('astilectron-ready', function() {
        installer.app = astilectron;

        installer.init();
    })
}

function finalize() {
    toggleButton(installer.nextButton, false);
    document.getElementById("progressSpinner").style.visibility = "hidden";
}

function updateProgress(percentage, message) {
    document.getElementById("progressMessage").textContent = message;

    if (percentage == 100) {
        finalize();
    }
}

function checkPasswords(password, confirm) {
    password = password ? password : "";
    confirm = confirm ? confirm : "";

    if (password.length < 8) {
        installer.showError("Enter a password longer than 8 characters");
    } else if (password != confirm) {
        installer.showError("Passwords do not match");
    } else {
        return true;
    }

    return false;
}

function toggleEncryptionPassword() {
    let checkbox = document.getElementById("encryptField");

    let password = document.getElementById("encryptPasswordField");
    let confirmPassword = document.getElementById("encryptPasswordConfirm");

    if (checkbox.checked == true) {
        password.disabled = false;
        confirmPassword.disabled = (password.value == "");
    } else {
        password.value = "";
        confirmPassword.value = "";
        password.disabled = true;
        confirmPassword.disabled = true;
    }
}

function toggleButton(button, disabled) {
    if (disabled) {
        button.disabled = true;
        button.style.cursor = "not-allowed";
        button.style.opacity = 0.6;
        return
    }

    button.disabled = false;
    button.style.cursor = "auto";
    button.style.opacity = 1;
}
