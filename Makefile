GOPATH ?= $(HOME)/go
PATH := $(GOPATH)/bin:$(PATH)
DESTDIR ?= /

installdir ?= /usr/share/redfield/apps/installer

.PHONY: all
all: bins

.PHONY: bins
bins:
	mkdir -p bin
	go build -o bin/installer cmd/installer/*.go
	go build -o bin/gui-installer cmd/gui-installer/*.go

.PHONY: clean
clean:
	rm -rf bin/
	rm -f conf/installer.yaml

.PHONY: deps
deps:
	go get ./...

.PHONY: fmt
fmt:
	find cmd/ pkg/ -name '*.go' | xargs gofmt -w -s

.PHONY: vendor
vendor:
	GO111MODULE=on go mod vendor

.PHONY: test
test:
	@echo "TODO: we should have some tests"

.PHONY: install-bins
install-bins: bins
	install -d -m 0755 $(DESTDIR)/$(installdir)
	install -m 0755 bin/gui-installer $(DESTDIR)/$(installdir)/gui-installer
	install -m 0755 bin/installer $(DESTDIR)/$(installdir)/installer

conf/installer.yaml:
	sed -e "s:@APP_PATH@:\"$(installdir)\":" $@.in > $@

.PHONY: install-confs
install-confs: conf/installer.yaml
	mkdir -p $(DESTDIR)/$(sysconfdir)
	install -m 0644 conf/installer.yaml $(DESTDIR)/$(sysconfdir)
	install -d -m 0755 $(DESTDIR)/$(installdir)/vendor
	install -m 0644 conf/status.json $(DESTDIR)/$(installdir)/vendor/

.PHONY:
install-electron-deps:
	install -d -m 0755 $(DESTDIR)/$(installdir)/app
	install -m 0644 app/index.html $(DESTDIR)/$(installdir)/app/
	install -d -m 0755 $(DESTDIR)/$(installdir)/app/static/js
	install -d -m 0755 $(DESTDIR)/$(installdir)/app/static/css
	install -m 0644 app/static/js/* $(DESTDIR)/$(installdir)/app/static/js/
	install -m 0644 app/static/css/* $(DESTDIR)/$(installdir)/app/static/css/

.PHONY: install
install: install-bins install-confs install-electron-deps

.PHONY: check
check: golint all test
	DESTDIR=/tmp make install
	go install `go list -f  "{{.ImportPath}}" "{{.TestGoFiles}}" ./...`

.PHONY: golint
golint:
	golangci-lint --verbose run --enable-all -Dgochecknoglobals -Dgochecknoinits -Dlll -Dfunlen

