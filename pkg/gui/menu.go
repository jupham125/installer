// Copyright © 2019 Assured Information Security, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package gui

import (
	"encoding/json"
	"log"

	"github.com/asticode/go-astilectron"
	"github.com/pkg/errors"

	"gitlab.com/redfield/installer/pkg/installer"
)

// InstallerGUI Represents the state of the installer UI
type UI struct {
	Config *installer.InstallInfo
	App    *astilectron.Astilectron
	Window *astilectron.Window
}

// UIMessage Represents a JSON-encoded message sent between the installer and UI JavaScript
type uiMessage struct {
	Type string      `json:"type"`
	Data interface{} `json:"data"`
}

// Save raw bytes of message payload for later unmarshaling into instances of other types
func (message *uiMessage) UnmarshalJSON(data []byte) error {
	aux := &struct {
		Type string
		Data json.RawMessage
	}{}

	if err := json.Unmarshal(data, aux); err != nil {
		return err
	}

	message.Type = aux.Type
	message.Data = aux.Data

	return nil
}

// Close Close the UI window
func (gui UI) Close() {
	if gui.Window != nil {
		gui.Window.Close()
	}

	if gui.App != nil {
		gui.App.Close()
	}
}

// UpdateProgress Update the progress indicator on the UI progress screen
func (gui UI) UpdateProgress(percent int, message string) {
	err := gui.sendMessage("progress", map[string]interface{}{
		"percentage": percent,
		"message":    message,
	})

	if err != nil {
		log.Printf("%v", err)
	}
}

func (gui UI) sendError(err error) error {
	return gui.sendMessage("error", err.Error())
}

func (gui UI) sendMessage(mType string, data interface{}) error {
	message, err := json.Marshal(uiMessage{mType, data})
	if err != nil {
		return err
	}

	if err := gui.Window.SendMessage(string(message)); err != nil {
		return err
	}

	return nil
}

// NewUI Scaffold installer UI astilectron window with options
func NewUI(i *installer.InstallInfo) (*UI, error) {
	var err error

	gui := UI{
		Config: i,
	}

	appOptions := astilectron.Options{
		AppName:           gui.Config.ProductName + " Installer",
		BaseDirectoryPath: gui.Config.AppPath,
	}

	if gui.App, err = astilectron.New(appOptions); err != nil {
		return nil, errors.Wrap(err, "failed to create astilectron app")
	}

	if err = gui.App.Start(); err != nil {
		return nil, errors.Wrap(err, "failed to start astilectron app")
	}

	windowOptions := &astilectron.WindowOptions{
		Center: astilectron.PtrBool(true),
		Height: astilectron.PtrInt(650),
		Width:  astilectron.PtrInt(875),
	}

	if gui.Window, err = gui.App.NewWindow(gui.Config.AppPath+"/app/index.html", windowOptions); err != nil {
		gui.App.Close()
		return nil, err
	}

	if err = gui.Window.Create(); err != nil {
		return nil, errors.Wrap(err, "failed to create electron window")
	}

	return &gui, nil
}

// Run Spawn the installer UI window, send default state JSON
func Run(i *installer.InstallInfo) error {
	gui, err := NewUI(i)
	if err != nil {
		return err
	}

	defer gui.Close()

	if err != nil {
		return err
	}

	// Handle messages from UI JS, set windowErr when errors occur
	var windowErr error
	handleWindowMessage := func(message *astilectron.EventMessage) interface{} {
		m := uiMessage{}
		windowErr = message.Unmarshal(&m)
		if windowErr != nil {
			return nil
		}

		switch m.Type {
		case "submit":
			mJSON, ok := m.Data.(json.RawMessage)
			if !ok {
				windowErr = errors.New("unable to retrieve UI state json")
				return nil
			}

			if windowErr = json.Unmarshal(mJSON, gui.Config); windowErr != nil {
				windowErr = err
				return nil
			}

			if windowErr = PerformInstallWithGUI(*gui.Config, gui); windowErr != nil {
				windowErr = gui.sendError(windowErr)
				return nil
			}
		case "finish":
			Reboot()
		}

		return nil
	}

	gui.Window.OnMessage(handleWindowMessage)

	if err = gui.sendMessage("state", i); err != nil {
		return errors.Wrap(err, "failed to send UI state")
	}

	gui.App.Wait()

	if err == nil && windowErr != nil {
		return windowErr
	}

	return err
}
