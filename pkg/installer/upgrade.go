// Copyright © 2018 Assured Information Security, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package installer

import (
	"log"
	"strings"
)

func changeFileDestinations(info *InstallInfo) {
	for i, f := range info.InstallFiles {
		info.InstallFiles[i].DestinationFile = strings.ReplaceAll(f.DestinationFile, "root", "upgrade")
		for j, m := range f.Mounts {
			info.InstallFiles[i].Mounts[j].Source = strings.ReplaceAll(m.Source, "root", "upgrade")
		}
	}
}

func initializeLVM() error {
	err := ScanPhysicalVolumes()
	if err != nil {
		return err
	}

	err = ScanVolumeGroups()
	if err != nil {
		return err
	}

	err = ActivateVolumeGroups()
	if err != nil {
		return err
	}

	err = ScanLogicalVolumes()
	if err != nil {
		return err
	}

	return nil
}

// PerformUpgrade Creates an installation according to the information in InstallInfo
func PerformUpgrade(i InstallInfo) error {
	err := initializeLVM()
	if err != nil {
		return err
	}

	err = OpenDiskCrypts(i.DiskCrypts, true)
	if err != nil {
		log.Printf("Opening encrypted disk partitions failed, this may be due to them already being decrypted, so continuing... %v", err)
	}

	err = CreateFileSystems(i.FileSystems, "upgrade")
	if err != nil {
		return err
	}

	err = ConfigureEFI(i)
	if err != nil {
		return err
	}

	// Substitute the upgrade logical volume as
	// as the install target, rather than root
	changeFileDestinations(&i)

	err = PlaceFiles(i)
	if err != nil {
		return err
	}

	err = RenameLogicalVolume(i.PhysicalVolumes, "dom0", "root", "oldroot")
	if err != nil {
		return err
	}

	err = RenameLogicalVolume(i.PhysicalVolumes, "dom0", "upgrade", "root")
	if err != nil {
		return err
	}

	err = RenameLogicalVolume(i.PhysicalVolumes, "dom0", "oldroot", "upgrade")
	if err != nil {
		return err
	}

	return nil
}
