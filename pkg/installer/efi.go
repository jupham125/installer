// Copyright 2018 Assured Information Security, Inc. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package installer

import (
	"fmt"
	"io/ioutil"
	"os/exec"
	"strings"

	"github.com/pkg/errors"
)

// EFIEntry EFI Configuration boot entry
type EFIEntry struct {
	Name    string `yaml:"name"`
	Options string `yaml:"options"`
	Kernel  string `yaml:"kernel"`
	RAMDisk string `yaml:"ramdisk"`
}

// EFIConfiguration EFI boot configuration
type EFIConfiguration struct {
	Default     string     `yaml:"default"`
	Destination string     `yaml:"destination"`
	EFIEntries  []EFIEntry `yaml:"efi_entries,flow"`
}

// WriteEFIConfig Write an EFI boot configuration
func WriteEFIConfig(efiConfig EFIConfiguration) error {
	output := "[global]\n"
	output += "default=" + efiConfig.Default + "\n\n"

	for _, entry := range efiConfig.EFIEntries {
		output += "[" + entry.Name + "]\n"
		output += "options=" + entry.Options + "\n"
		output += "kernel=" + entry.Kernel + "\n"
		output += "ramdisk=" + entry.RAMDisk + "\n\n"
	}

	err := ioutil.WriteFile(efiConfig.Destination, []byte(output), 0644)
	if err != nil {
		return errors.Wrapf(err, "failed to write EFI Configuration. \n %v", output)
	}

	return nil
}

// CleanEFIBootEntries Clear all EFI boot targets
func CleanEFIBootEntries(i InstallInfo) error {
	efiBootEntries, err := exec.Command("efibootmgr").CombinedOutput()
	if err != nil {
		return errors.Wrapf(err, "failed to run efibootmgr: %s", efiBootEntries)
	}

	for _, l := range strings.Split(strings.TrimSpace(string(efiBootEntries)), "\n") {
		var id int
		var lbl string
		n, err := fmt.Sscanf(l, "Boot%X* %s", &id, &lbl)
		if err != nil {
			// This line does not describe a boot entry, skip it.
			continue
		}

		if n == 2 && lbl == i.ProductName {
			efiRemove, err := exec.Command("efibootmgr", "-b", fmt.Sprintf("%04X", id), "-B").CombinedOutput()
			if err != nil {
				return errors.Wrapf(err, "failed to remove boot entry: %s", efiRemove)
			}
		}
	}

	return nil
}

// CreateEFIBootEntry Add a boot entry for this installation
func CreateEFIBootEntry(i InstallInfo) error {
	registerEfiBootEntry, err := exec.Command("efibootmgr",
		"--create", "--disk", i.BlockDevice, "--part", "1",
		"--loader", "/EFI/BOOT/BOOTX64.EFI", "--label", i.ProductName,
		"--verbose").CombinedOutput()
	if err != nil {
		return errors.Wrapf(err, "failed to create EFI Boot entry: %s", registerEfiBootEntry)
	}

	return nil
}

// ConfigureEFI Set up an EFI boot for this installation
func ConfigureEFI(i InstallInfo) error {
	err := CleanEFIBootEntries(i)
	if err != nil {
		return err
	}

	err = CreateEFIBootEntry(i)
	if err != nil {
		return err
	}

	err = WriteEFIConfig(i.EFIConfig)
	if err != nil {
		return err
	}

	return nil
}
