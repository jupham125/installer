// Copyright © 2018 Assured Information Security, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package installer

import (
	"fmt"
	"os/exec"
	"path/filepath"
	"strings"

	"github.com/pkg/errors"
)

// LogicalVolume Describes an LVM logical volume
type LogicalVolume struct {
	Name       string `yaml:"name"`
	Size       string `yaml:"size"`
	Percentage bool   `yaml:"percentage"`
}

func (l LogicalVolume) String() string {
	return fmt.Sprintf("Name: %v\nSize: %v\nPercentage: %v", l.Name, l.Size, l.Percentage)
}

// VolumeGroup Describes an LVM volume group
type VolumeGroup struct {
	Name           string          `yaml:"name"`
	LogicalVolumes []LogicalVolume `yaml:"logical_volumes,flow"`
}

func (v VolumeGroup) String() string {
	return fmt.Sprintf("Name: %v\nLogical Volumes: %v", v.Name, v.LogicalVolumes)
}

// PhysicalVolume Describes an LVM physical volume
type PhysicalVolume struct {
	PartitionLabel string        `yaml:"partition_label"`
	VolumeGroups   []VolumeGroup `yaml:"volume_groups,flow"`
}

func (p PhysicalVolume) String() string {
	return fmt.Sprintf("PartitionName: %v\nVolume Groups: %v", p.PartitionLabel, p.VolumeGroups)
}

// ScanPhysicalVolumes
func ScanPhysicalVolumes() error {
	scanPhysicalVolumes, err := exec.Command("pvscan").CombinedOutput()
	if err != nil {
		return errors.Wrapf(err, "failed to scan physical volumes: \n%s", scanPhysicalVolumes)
	}

	return nil
}

func createPhysicalVolume(p PhysicalVolume) error {
	if p.PartitionLabel == "" {
		return errors.New("empty partition label provided")
	}

	path := filepath.Join("/dev/disk/by-partlabel/", p.PartitionLabel)

	createPhysicalVolume, err := exec.Command("pvcreate", "-ff", "-y", path).CombinedOutput()
	if err != nil {
		return errors.Wrapf(err, "failed to create physical volume: \n%s", createPhysicalVolume)
	}

	activatePhysicalVolume, err := exec.Command("pvscan").CombinedOutput()
	if err != nil {
		return errors.Wrapf(err, "failed to scan physical volume: \n%s", activatePhysicalVolume)
	}

	return nil
}

// CreatePhysicalVolumes Creates a set of physical volumes
func CreatePhysicalVolumes(pvs []PhysicalVolume) error {
	for _, pv := range pvs {
		err := createPhysicalVolume(pv)
		if err != nil {
			return err
		}
	}

	return nil
}

// ScanVolumeGroups
func ScanVolumeGroups() error {
	scanVolumeGroups, err := exec.Command("vgscan").CombinedOutput()
	if err != nil {
		return errors.Wrapf(err, "failed to scan volume groups: \n%s", scanVolumeGroups)
	}

	return nil
}

// ScanVolumeGroups
func ActivateVolumeGroups() error {
	activateVolumeGroups, err := exec.Command("vgchange", "-ay").CombinedOutput()
	if err != nil {
		return errors.Wrapf(err, "failed to activate volume groups: \n%s", activateVolumeGroups)
	}

	return nil
}

func createVolumeGroup(pv PhysicalVolume, vg VolumeGroup) error {
	if pv.PartitionLabel == "" {
		return errors.New("empty partition label provided")
	}

	path := filepath.Join("/dev/disk/by-partlabel/", pv.PartitionLabel)

	createVolumeGroup, err := exec.Command("vgcreate", vg.Name, path).CombinedOutput()
	if err != nil {
		return errors.Wrapf(err, "failed to create volume group: \n%s", createVolumeGroup)
	}

	err = ScanVolumeGroups()
	if err != nil {
		return errors.Wrapf(err, "failed to create volume group: \n%v", createVolumeGroup)
	}

	return nil
}

// CreateVolumeGroups Creates a set of volume groups within a set of physical volumes
func CreateVolumeGroups(pvs []PhysicalVolume) error {
	for _, pv := range pvs {
		for _, vg := range pv.VolumeGroups {
			err := createVolumeGroup(pv, vg)
			if err != nil {
				return err
			}
		}
	}

	return ActivateVolumeGroups()
}

func createLogicalVolume(vg VolumeGroup, lv LogicalVolume) error {
	var sizeString string

	if strings.Contains(lv.Size, "%") {
		sizeString = fmt.Sprintf("-l%s", lv.Size)
	} else {
		sizeString = fmt.Sprintf("-L%s", lv.Size)
	}

	createLogicalVolume, err := exec.Command("lvcreate", "-y", "--name", lv.Name, sizeString, vg.Name).CombinedOutput()
	if err != nil {
		return errors.Wrapf(err, "failed to create logical volume: \n%s", createLogicalVolume)
	}

	return nil
}

// CreateLogicalVolumes Creates a set of logical volumes within a set of physical volumes and volume groups
func CreateLogicalVolumes(pvs []PhysicalVolume) error {
	for _, pv := range pvs {
		for _, vg := range pv.VolumeGroups {
			for _, lv := range vg.LogicalVolumes {
				err := createLogicalVolume(vg, lv)
				if err != nil {
					return err
				}
			}
		}
	}

	return nil
}

func renameLogicalVolume(vgName string, oldName string, newName string) error {
	renameLogicalVolume, err := exec.Command("lvrename", "-y", vgName, oldName, newName).CombinedOutput()
	if err != nil {
		return errors.Wrapf(err, "failed to rename logical volume: \n%s", renameLogicalVolume)
	}

	return nil
}

// ScanLogicalVolumes
func ScanLogicalVolumes() error {
	scanLogicalVolumes, err := exec.Command("lvscan").CombinedOutput()
	if err != nil {
		return errors.Wrapf(err, "failed to scan logical volumes: \n%s", scanLogicalVolumes)
	}

	return nil
}

// RenameLogicalVolume
func RenameLogicalVolume(pvs []PhysicalVolume, vgName string, oldName string, newName string) error {
	for _, pv := range pvs {
		for _, vg := range pv.VolumeGroups {
			if vg.Name == vgName {
				return renameLogicalVolume(vg.Name, oldName, newName)
			}
		}
	}

	return errors.New("No matching source volume found")
}
