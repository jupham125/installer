module gitlab.com/redfield/installer

go 1.13

require (
	github.com/asticode/go-astilectron v0.8.0
	github.com/asticode/go-astilog v1.0.0
	github.com/asticode/go-astitools v1.2.0 // indirect
	github.com/kr/pretty v0.1.0 // indirect
	github.com/pkg/errors v0.8.1
	golang.org/x/crypto v0.0.0-20191002192127-34f69633bfdc
	golang.org/x/net v0.0.0-20190620200207-3b0461eec859 // indirect
	gopkg.in/check.v1 v1.0.0-20180628173108-788fd7840127 // indirect
	gopkg.in/yaml.v2 v2.2.4
)
